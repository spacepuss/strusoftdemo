﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StruSoftDemo.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace StruSoftDemo.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IWebHostEnvironment _env;
        public string ApiVersion { get; private set; }
        public string ServiceName { get; private set; }
        public List<WeatherForecast> WeatherForecast { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
            ApiVersion = Environment.GetEnvironmentVariable("STRUSOFT_API_VERSION");
            ServiceName = Environment.GetEnvironmentVariable("STRUSOFT_SERVICE_NAME");
        }

        public async Task OnGet()
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage();
                request.RequestUri = new Uri($"http://{ServiceName}/WeatherForecast");
                var response = await client.SendAsync(request);
                var result = await response.Content.ReadAsStringAsync();
                WeatherForecast = JsonConvert.DeserializeObject<List<WeatherForecast>>(result);
            }
        }
    }
}
